from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'app.views.landing_page'),
    url(r'^signup/$', 'app.views.signup'),
    url(r'^activate/(?P<code>[\w\-]+)/$', 'app.views.activate'),
    url(r'^dashboard/$', 'app.views.index'),
    url(r'^api/', include('app.api_urls')),
    url(r'^upload-profile-image/$', 'app.api.profile_image_upload'),
    url(r'partials/(?P<template_name>.*)$', 'app.views.load_partial'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    )