app.factory('User', function ($resource) {
    return $resource(
        '/api/user/:listCtrl:id/:docCtrl/', {
            id: '@id',
            listCtrl: '@listCtrl',
            docCtrl: '@docCtrl'
        }, {
            update: {
                method: 'PUT'
            },
            full: {
                method: 'GET',
                params: {
                    listCtrl: 'full',
                    id: '@id'
                }
            },
            current: {
                method: 'GET',
                params: {
                    listCtrl: 'current'
                }
            },
            nearby: {
                method: 'GET',
                params: {
                    docCtrl: 'nearby'
                },
                isArray: true
            }
        }
    );
});

app.factory('Feed', function ($resource) {
    return $resource('/api/feed/:id:listCtrl/:docCtrl/', {
            id: '@id',
            listCtrl: '@listCtrl',
            docCtrl: '@docCtrl'
        }, {
            update: {
                method: 'PUT'
            }
        }
    );
});

app.factory('Message', function ($resource) {
    return $resource(
        '/api/message/:id:listCtrl/:docCtrl/', {
            id: '@id',
            listCtrl: '@listCtrl',
            docCtrl: '@docCtrl'
        }, {
            update: {
                method: 'PUT'
            }
        }
    );
});

app.factory('Thread', function ($resource) {
    return $resource(
        '/api/thread/:id:listCtrl/:docCtrl/', {
            id: '@id'
        }, {
            update: {
                method: 'PUT'
            },
            full: {
                method: 'GET',
                params: {
                    docCtrl: 'full',
                    id: '@id'
                },
                isArray: true
            },
            complete: {
                method: 'GET',
                params: {
                    docCtrl: 'complete',
                    id: '@id'
                }
            }
        }
    );
});