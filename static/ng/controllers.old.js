/*function HomeCtrlOld($scope, $http) {
 function getLocation() {
 if (navigator.geolocation) {
 navigator.geolocation.getCurrentPosition(getPosition);
 }
 else {
 x.innerHTML = "Geolocation is not supported by this browser.";
 }
 }

 function getPosition(position) {
 $scope.user.lat = position.coords.latitude;
 $scope.user.lon = position.coords.longitude;
 }

 $scope.getFeed = function () {
 $http.get('/api/feed/nearby/').success(function(data){
 $scope.feed = data;
 })
 }

 $scope.getFollowerFeed = function () {
 $http.get('/api/feed/followers/').success(function(data){
 $scope.feed = data;
 console.log($scope.feed);
 })
 }

 $http.get('/api/get-user/').success(function (data) {
 $scope.user = data;
 //console.log($scope.user);
 //getLocation();
 $http.put('/api/generics/user/' + $scope.user.id, $scope.user);
 $scope.getFeed();
 })

 $scope.message = {};
 $scope.postToFeed = function() {
 $scope.message.user = $scope.user.id;
 console.log($scope.message);
 $http.post('/api/generics/feed/', $scope.message);
 $scope.message = null;
 //$scope.getFeed();
 return;
 }

 $scope.postForLeisure = function() {
 $scope.message.user = $scope.user.id;
 $scope.message.for_leisure = true;
 $http.post('/api/generics/feed/', $scope.message);
 $scope.message = null;
 //$scope.getFeed();
 return;
 }

 $scope.postForNetworking = function() {
 $scope.message.user = $scope.user.id;
 $scope.message.for_networking = true;
 $http.post('/api/generics/feed/', $scope.message);
 $scope.message = null;
 //$scope.getFeed();
 return;
 }
 }*/

function HomeCtrl($scope, Feed, $http) {
    //$scope.profile = null;
    $scope.cInterval = -1;

    /*$scope.getNext = function (items, lastHash) {
     if (lastHash) {
     for (var i = 0; i < items.length; i++) {
     if (items[i].id === lastHash) {
     return items[i + 1];
     }
     }
     } else {
     return items[0];
     }
     };*/

    /*$scope.user = User.current(
     function () {
     $scope.nearby = User.nearby({
     id: $scope.user.id
     }, function() {
     //$scope.profile = $scope.getNext($scope.nearby);
     });
     }
     )*/

    $scope.reply = null;

    $scope.postReply = function (feedId, reply) {
        var feed = Feed.get({
            id: feedId
        }, function () {
            /*var thread = new Thread();
             thread.subject = feed.text;
             thread.from_user = $scope.user.id;
             thread.to_user = feed.user;
             thread.$save()
             console.log(thread);*/
            var thread = {};
            thread.subject = feed.text;
            thread.from_user = $scope.user.id;
            thread.to_user = feed.user;
            thread.is_deleted = false;
            console.log(thread);
            $http.post('/api/thread/', thread).success(function (thread) {
                var msg = {};
                msg.thread = thread.id;
                msg.text = reply;
                msg.from_user = $scope.user.id;
                $http.post('/api/message/', msg).success(function (msg) {
                    //console.log(msg);
                    //console.log($scope.reply);
                    $scope.reply = null;
                });
            });

        })
    };

}

function PostFeedCtrl($scope, $http) {
    $scope.feedText = '';

    $scope.postFeed = function () {
        var post = {};
        post.text = $scope.feedText;
        post.user = $scope.user.id;
        $http.post('/api/feed/', post).success(function (post) {
            console.log(post);
            $scope.feedText = null;
        })
    };
}

function MsgListCtrl($scope, Thread) {
    //console.log('i am here');
    $scope.threads = Thread.full({
        id: $scope.user.id
    })
}

function MsgViewCtrl($scope, $routeParams, Thread, $http) {
    console.log($routeParams.threadID);
    $scope.thread = Thread.complete({
        id: $routeParams.threadID
    });

    $scope.postMsg = function (msgText) {
        var msg = {

        };
        msg.text = msgText;
        msg.from_user = $scope.user.id;
        msg.thread = $routeParams.threadID;
        $http.post('/api/message/', msg).success(function (msg) {
            console.log(msg);
            $scope.msgText = null;
            $scope.thread = Thread.complete({
                id: $routeParams.threadID
            });
        });
    };

    $scope.getMsgUser = function (userID) {
        if (userID == $scope.thread.from_user.id) {
            return $scope.thread.from_user
        } else {
            return $scope.thread.to_user
        }
    }
}

function ProfileCtrl($scope, User, Feed) {
    $scope.bar = function (content) {
        console.log(content);
        $scope.user = User.current();
        console.log($scope.user);
        //$scope.user = content;

    };

    $scope.deleteFeed = function (feed) {
        console.log(feed);
        var position = $scope.user.feed.indexOf(feed);
        console.log(position);
        Feed.delete({
            id: feed.id
        });
        console.log($scope.user.feed);
        $scope.user.feed.splice(position, 1);
        console.log($scope.user.feed);
    };

    //for ng-show/ng-hide .. if true, show profile, if false show the form
    $scope.showProfile = true;

    $scope.showEditProfile = function (val) {
        val = !val;
        $scope.showProfile = val;
    };

    //advance editing of profile
    $scope.showQuestions = true;

}

