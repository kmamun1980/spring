var app = angular.module('app', ['ngResource', 'ui.bootstrap', 'ngUpload']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/partials/home.html',
            controller: HomeCtrl
        }).when('/messages/', {
            templateUrl: '/partials/msgList.html',
            controller: MsgListCtrl
        }).when('/messages/:threadID/', {
            templateUrl: 'partials/msgView.html',
            controller: MsgViewCtrl
        }).when('/profile/', {
            templateUrl: 'partials/profile.html',
            controller: ProfileCtrl
        }).otherwise({
            redirectTo: '/'
        })
}).config(function ($httpProvider) {
        $httpProvider.defaults.headers.common['X-CSRFToken'] = window.document.cookie.match(/csrftoken=([^;]+)/)[1];
        console.log(window.document.cookie.match(/csrftoken=([^;]+)/)[1]);
    });

app.run(function($rootScope, User, $location){
    $rootScope.static_url = 'http://localhost:7777/spring/static/';

    $rootScope.media_url = 'http://localhost:7777/spring/media/';

    $rootScope.user = User.current(
        function () {
            //console.log('iamhere');
            $rootScope.nearby = User.nearby({
                    id: $rootScope.user.id
                }, function() {
                //$scope.profile = $scope.getNext($scope.nearby);
            });
        }
    );

    $rootScope.showHomeMenu = function () {
        //console.log($location.path());
        return $location.path() == '/';
    };

    $rootScope.fullMediaPath = function(partialMediaPath) {
        //console.log(partialMediaPath);
        return $rootScope.media_url + partialMediaPath;
    };

    $rootScope.getAge = function(dob) {
        dob = dob.split('-')[0];
        //console.log(dob);
        var now = new Date().getFullYear();
        //console.log(now);
        return now-dob;
    }
})