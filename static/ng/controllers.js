function HomeCtrl($scope, Feed, $http) {
    //cInterval for ui-carousal
    $scope.cInterval = -1;

    //the reply box on each feed item
    $scope.reply = null;


    $scope.postReply = function (feedId, reply) {
        // get the feed ... the function can be replaced with
        // Feed.get().success().error()
        var feed = Feed.get({id: feedId}, function () {
            var thread = {};
            thread.subject = feed.text;
            thread.from_user = $scope.user.id;
            thread.to_user = feed.user;
            thread.is_deleted = false;
            console.log(thread);
            $http.post("/api/thread/", thread).success(function (thread) {
                var msg = {};
                msg.thread = thread.id;
                msg.text = reply;
                msg.from_user = $scope.user.id;
                $http.post("/api/message/", msg).success(function (msg) {
                    $scope.reply = null
                })
            })
        })
    }
}


function PostFeedCtrl($scope, $http) {
    $scope.feedText = "";
    $scope.postFeed = function () {
        var post = {};
        post.text = $scope.feedText;
        post.user = $scope.user.id;
        $http.post("/api/feed/", post).success(function (post) {
            console.log(post);
            $scope.feedText = null
        })
    }
}


function MsgListCtrl($scope, Thread) {
    $scope.threads = Thread.full({id: $scope.user.id})
}


function MsgViewCtrl($scope, $routeParams, Thread, $http) {
    console.log($routeParams.threadID);
    $scope.thread = Thread.complete({id: $routeParams.threadID});
    $scope.postMsg = function (msgText) {
        var msg = {};
        msg.text = msgText;
        msg.from_user = $scope.user.id;
        msg.thread = $routeParams.threadID;
        $http.post("/api/message/", msg).success(function (msg) {
            console.log(msg);
            $scope.msgText = null;
            $scope.thread = Thread.complete({id: $routeParams.threadID})
        })
    };
    $scope.getMsgUser = function (userID) {
        if (userID == $scope.thread.from_user.id) {
            return $scope.thread.from_user
        } else {
            return $scope.thread.to_user
        }
    }
}


function ProfileCtrl($scope, User, Feed) {
    $scope.bar = function (content) {
        console.log(content);
        $scope.user = User.current();
        console.log($scope.user)
    };
    $scope.deleteFeed = function (feed) {
        console.log(feed);
        var position = $scope.user.feed.indexOf(feed);
        console.log(position);
        Feed["delete"]({id: feed.id});
        console.log($scope.user.feed);
        $scope.user.feed.splice(position, 1);
        console.log($scope.user.feed)
    };
    $scope.showProfile = true;
    $scope.showEditProfile = function (val) {
        val = !val;
        $scope.showProfile = val
    };

    $scope.saveProfile = function(val) {
        $scope.showEditProfile(val);

    };

    $scope.showQuestions = true;

    $scope.showEditQuestions = function (val) {
        val = !val;
        $scope.showQuestions = val
    };

    $scope.saveQuestions = function(val) {
        $scope.showEditQuestions(val);

    }
}
;