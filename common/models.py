from django.contrib.auth import user_logged_in
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.core.mail import send_mail
from django.db import models
from django.db.models import signals
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

#for activation link
import uuid, base64

#managers
from common.managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=128, unique=True)
    email = models.CharField(max_length=32, unique=True)
    birthday = models.DateField(blank=True, null=True)

    #questions
    currently_doing = models.TextField(blank=True, null=True)
    here_for = models.TextField(blank=True, null=True)
    interested_in = models.TextField(blank=True, null=True)
    fun_ideas = models.TextField(blank=True, null=True)
    good_at = models.TextField(blank=True, null=True)
    #like = models.TextField(blank=True, null=True)

    #picture
    picture = models.ImageField(upload_to='media/user', blank=True, null=True)
    thumb = models.ImageField(upload_to='media/user/thumbs', blank=True, null=True)

    #for django admin purposes
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'))
    #for activation and other stuff
    is_active = models.BooleanField(
        _('active'),
        default=False,
        help_text=_('Designates whether this user should be treated as '
                    'active. Un select this instead of deleting accounts.')
    )

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    #current location
    #lat = models.FloatField(default=0)
    #lon = models.FloatField(default=0)
    post_code = models.CharField(max_length=6, blank=True, null=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __unicode__(self):
        return self.email

    def get_short_name(self):
        return self.username

    def email_user(self, subject, message, from_email=None):
        send_mail(subject, message, from_email, self.email)


class Activation(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    code = models.CharField(max_length=128, unique=True)
    is_consumed = models.BooleanField(default=False)

    def get_code(self):
        if not self.code:
            #self.code = base64.urlsafe_b64encode(uuid.uuid4().bytes).replace('=', '')
            self.code = base64.urlsafe_b64encode(uuid.uuid4().bytes).translate(None, '=-_')
        return self.code

    def consume(self):
        self.user.is_active = True
        self.is_consumed = True
        self.user.save()


class AllowedDomain(models.Model):
    domain = models.CharField(max_length=128)

    def __unicode__(self):
        return self.domain


class Follower(models.Model):
    from_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='following')
    to_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='being_followed')
    is_blocked = models.BooleanField(default=False)


#signals
def update_last_login(sender, user, **kwargs):
    """
    A signal receiver which updates the last_login date for
    the user logging in.
    """
    user.last_login = timezone.now()
    user.save(update_fields=['last_login'])

user_logged_in.connect(update_last_login)


def create_activation_code(sender, instance, created, **kwargs):
    if created:
        if not instance.is_active:
            activation, created = Activation.objects.get_or_create(user=instance)
            activation.get_code()
            activation.save()
        else:
            print 'the signal fails to evaluate active state'

signals.post_save.connect(create_activation_code, sender=User)