from django.db.models import Manager, Q
import math


class ThreadManager(Manager):
    def get_all(self, user):
        return self.filter(Q(to_user = user) | Q(from_user = user)).order_by('-updated_on')


#class MessageManager(Manager):
#    def get_all(self, user):
#        return self.filter(Q(to_user = user) | Q(from_user = user)).order_by('-added_on')



class FeedManager(Manager):
    def get_by_distance(self, distance, lat, lon, user):
        R = 6371
        d = float(distance) / float(R)
        max_lat = lat + math.degrees(d)
        min_lat = lat - math.degrees(d)
        max_lon = lon + math.degrees(d)
        min_lon = lon - math.degrees(d)
        feed = self.filter(
            lat__lte = max_lat
        ).filter(
            lat__gte = min_lat
        ).filter(
            lon__gte = min_lon
        ).filter(
            lon__lte = max_lon
        ).exclude(user = user)
        return feed

    def get_by_interest(self, interest, user):
        pass

    def get_by_followers(self, user):
        followers = user.following.filter(is_blocked=False)
        feed = self.filter(user__in=followers).exclude(user=user)
        return feed

    def get_nearby(self, user):
        post_code = user.post_code
        feed = self.filter(user__post_code__contains=post_code)
        return feed