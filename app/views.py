from django.forms.formsets import formset_factory
from django.core.mail import EmailMessage
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import get_template
from django.template import Context, RequestContext
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import requires_csrf_token
from django.views.decorators.http import require_GET
from cms.models import *
from common.models import *
from app.forms import *
import uuid, base64


def landing_page(request):
    template = get_template('landing/index.html')
    images = Image.objects.order_by('-date_added').all()[:5]
    output = {
        'images': images,
    }
    return HttpResponse(template.render(RequestContext(request, output)))


def signup(request):
    template = get_template('landing/signup.html')
    email = request.GET['email']
    username = request.GET['username']
    user = User.objects.create_inactive_user(
        email=email,
        username=username,
    )
    activation_code = user.activation.code
    output = {
        'email': email,
        'username': username,
        'activation_code': activation_code,
    }
    return HttpResponse(template.render(RequestContext(request, output)))


def activate(request, code):
    template = get_template('landing/activation.html')
    code = code
    #email = request.GET['email']
    #username = request.GET['username']
    #user = User.objects.create_inactive_user(
    #    email=email,
    #    username=username,
    #)
    #activation_code = user.activation.code
    #output = {
    #    'email': email,
    #    'username': username,
    #    'activation_code': activation_code,
    #}
    output = {}
    return HttpResponse(template.render(RequestContext(request, output)))




@requires_csrf_token
@login_required()
def index(request):
    template = get_template('dashboard/base.html')
    output = {}
    return HttpResponse(template.render(RequestContext(request, output)))


@require_GET
def load_partial(request, template_name):
    if template_name:
        path = 'partials/%s' % template_name
        t = get_template(path)
        ctx = Context({})
        return HttpResponse(t.render(ctx))
    else:
        return HttpResponse('unknown partial template')

@csrf_exempt
@login_required()
def ajax_image_upload(request):
    file = request.FILES['file']
    file._name = base64.urlsafe_b64encode(uuid.uuid4().bytes).translate(None, '=-_') + '.JPG'
    user = request.user
    user.picture = file
    user.save()
    return HttpResponse('image-uploaded')

