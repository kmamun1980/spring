from rest_framework import serializers
from app.models import *
from common.models import *


class FeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feed


class FullFeedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feed
        depth = 1


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message


class FullMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        depth = 1


class MessageThreadSerializer(serializers.ModelSerializer):
    class Meta:
        model = MessageThread


class FullMessageThreadSerializer(serializers.ModelSerializer):
    messages = MessageSerializer(many=True)

    class Meta:
        model = MessageThread
        depth = 1


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        exclude = ['password']


class FullUserSerializer(serializers.ModelSerializer):
    feed = FeedSerializer(many=True)

    class Meta:
        model = User
        exclude = ['email', 'password']