from django.db import models
from app.managers import *
from django.conf import settings


class MessageThread(models.Model):
    subject = models.TextField()
    to_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='to')
    from_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='from')
    added_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now_add=True)
    is_deleted = models.BooleanField(default=False)
    objects = ThreadManager()

    def __unicode__(self):
        return self.subject


class Message(models.Model):
    thread = models.ForeignKey(MessageThread, blank=True, related_name='messages')
    text = models.TextField()
    from_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='message_by')
    added_on = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.text


class Feed(models.Model):
    text = models.TextField()
    date = models.DateField(auto_now=True)
    #lat = models.FloatField(default=0)
    #lon = models.FloatField(default=0)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='feed')
    for_networking = models.BooleanField(default=False)
    for_leisure = models.BooleanField(default=False)
    objects = FeedManager()

    def __unicode__(self):
        return self.text
