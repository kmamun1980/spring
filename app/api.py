import base64
import uuid

from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets, routers
from rest_framework.decorators import link
from rest_framework.response import Response
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer

from app.models import *
from app.serializers import *


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


#generic view sets
class UserViewSet(viewsets.ModelViewSet):
    model = User
    serializer_class = UserSerializer

    @link()
    def full(self, request, pk=None):
        user = self.get_object()
        serializer = FullUserSerializer(user)
        return Response(serializer.data)

    @link()
    def nearby(self, request, pk=None):
        user = self.get_object()
        queryset = User.objects.filter(post_code=user.post_code).exclude(id=user.id).order_by('last_login')
        return Response(FullUserSerializer(queryset).data)


class FeedViewSet(viewsets.ModelViewSet):
    model = Feed
    serializer_class = FeedSerializer

    def get_queryset(self):
        user = self.request.user
        queryset = Feed.objects.get_nearby(user)
        return queryset


class MessageThreadViewSet(viewsets.ModelViewSet):
    model = MessageThread
    serializer_class = MessageThreadSerializer

    def get_queryset(self):
        user = self.request.user
        queryset = MessageThread.objects.get_all(user)
        return queryset

    @link()
    def full(self, request, pk=None):
        queryset = MessageThread.objects.get_all(self.request.user)
        serializer = FullMessageThreadSerializer(queryset)
        return Response(serializer.data)

    @link()
    def complete(self, request, pk=None):
        thread = self.get_object()
        serializer = FullMessageThreadSerializer(thread)
        return Response(serializer.data)


class MessageViewSet(viewsets.ModelViewSet):
    model = Message
    serializer_class = MessageSerializer

# class FullUserViewSet(viewsets.ModelViewSet):
#     model = User
#     serializer_class = FullUserSerializer

#
# class AllowDomainViewSet(viewsets.ModelViewSet):
#     model = AllowedDomain
#
#
# class FullFeedViewSet(viewsets.ModelViewSet):
#     model = Feed
#     serializer_class = FullFeedSerializer
#
#
# class FeedViewSet(viewsets.ModelViewSet):
#     model = Feed
#
#
# router = routers.DefaultRouter()
# router.register(r'user', UserViewSet)
# router.register(r'allowed-domains', AllowDomainViewSet)
# router.register(r'feed/full', FullFeedViewSet)
# router.register(r'feed', FeedViewSet)


#custom api methods
@login_required()
def get_current_user(request):
    return JSONResponse(FullUserSerializer(request.user).data)


@csrf_exempt
@login_required()
def profile_image_upload(request):
    file = request.FILES['file']
    file._name = base64.urlsafe_b64encode(uuid.uuid4().bytes).translate(None, '=-_') + '.JPG'
    user = request.user
    user.picture = file
    user.save()
    return JSONResponse(FullUserSerializer(user).data)


#routers

router = routers.DefaultRouter()
router.register(r'user', UserViewSet)
router.register(r'feed', FeedViewSet)
router.register(r'thread', MessageThreadViewSet)
router.register(r'message', MessageViewSet)
#router.register(r'user/full', FullUserViewSet)
