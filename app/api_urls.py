from django.conf.urls import patterns, include, url
from api import router

urlpatterns = patterns(
    '',
    url(r'^user/current/$', 'app.api.get_current_user'),
    url(r'', include(router.urls)),
)
