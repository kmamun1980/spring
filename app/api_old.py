from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from app.models import *
from app.serializers import *


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


def check_email(request):
    pass


def check_username(request):
    pass


@login_required()
def get_user(request):
    user = request.user
    if request.method == 'GET':
        serializer = UserSerializer(user)
        return JSONResponse(serializer.data)


# @login_required()
# def get_feed(request, distance):
#     user = request.user
#     if request.method == 'GET':
#         feed = Feed.objects.get_by_distance(distance, user.lat, user.lon, user)
#         serializer = FullFeedSerializer(feed)
#         return JSONResponse(serializer.data)


@login_required()
def get_feed_by_followers(request):
    user = request.user
    if request.method == 'GET':
        feed = Feed.objects.get_by_followers(user)
        serializer = FullFeedSerializer(feed)
        return JSONResponse(serializer.data)


@login_required()
def get_nearby_feed(request):
    user = request.user
    if request.method == 'GET':
        feed = Feed.objects.get_nearby(user=user)
        serializer = FullFeedSerializer(feed)
        return JSONResponse(serializer.data)

